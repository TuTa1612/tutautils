package com.assistcard.core.chat.database;

import java.util.List;

public interface ChatDAOListener {
	void onDAOResult(List<MessageDB> mensajes);
}
