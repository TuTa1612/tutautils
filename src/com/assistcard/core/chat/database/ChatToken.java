package com.assistcard.core.chat.database;

import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "ChatToken")
public class ChatToken {
		
	public static final String TABLE_NAME = "ChatToken";
	
	@DatabaseField(columnName = "DocumentoId", id = true)
	private String documentoId;
	@DatabaseField(columnName = "ChatId")
	private String chatId;
	
	public ChatToken() {
	}
	
	public ChatToken(String documentoId, String chatId) {
		this.documentoId = documentoId;
		this.chatId = chatId;
	}
	public String getDocumentoId() {
		return documentoId;
	}
	public void setDocumentoId(String documentoId) {
		this.documentoId = documentoId;
	}
	public String getChatId() {
		return chatId;
	}
	public void setChatId(String chatId) {
		this.chatId = chatId;
	}
	
	private static final String TABLE_CREATE = "CREATE TABLE "
		    + TABLE_NAME +
	    	" (DocumentoId            TEXT PRIMARY KEY,"+
	    	"  ChatId                 TEXT)";
	    
	    public static void onCreate(SQLiteDatabase database) {
	    	database.execSQL(TABLE_CREATE);
	    }

	    public static void onUpgrade(SQLiteDatabase database, int oldVersion,int newVersion) {
			database.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
			onCreate(database);
	    }	
}
