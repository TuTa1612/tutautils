package com.assistcard.core.chat.database;

import java.sql.SQLException;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;

/**
 * 
 * @author Cesar Cardozo
 * 
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {
	
	public static final String TAG = "database_error";
	// Database Data
	public static final String DATABASE_NAME = "asist_card.db";
	private static final int DATABASE_VERSION = 5;
	private static SQLiteDatabase db = null;
	
	private Dao<Vuelo, Long> vuelosDao = null;
	private Dao<ChatToken, String> chatTokenDao = null;
	private Dao<MessageDB, Long> chatDao = null;
	
	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
		Vuelo.onCreate(db);
		ChatToken.onCreate(db);
		MessageDB.onCreate(db);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource,int oldVersion, int newVersion) {
		Vuelo.onUpgrade(db, oldVersion, newVersion);
		ChatToken.onUpgrade(db, oldVersion, newVersion);
		MessageDB.onUpgrade(db, oldVersion, newVersion);
	}
	
	public synchronized SQLiteDatabase getDb() {
		return db;
	}
	/**
	 * Close the database connections and clear any cached DAOs.
	 */
	@Override
	public void close() {
		super.close();
		vuelosDao = null;
		chatTokenDao = null;
		chatDao = null;
	}
	
	public Dao<Vuelo, Long> getVuelosDao() throws SQLException {
		if (vuelosDao == null) {
			vuelosDao = getDao(Vuelo.class);
		}
		return vuelosDao;
	}
	
	public Dao<ChatToken, String> getChatTokenDao() throws SQLException {
		if (chatTokenDao == null) {
			chatTokenDao = getDao(ChatToken.class);
		}
		return chatTokenDao;
	}
	
	public Dao<MessageDB, Long> getChatDao() throws SQLException {
		if (chatDao == null) {
			chatDao = getDao(MessageDB.class);
		}
		return chatDao;
	}
}
