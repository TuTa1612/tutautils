package com.assistcard.core.chat.database;

import java.util.GregorianCalendar;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "Vuelos")
public class Vuelo {
	
	public static final String TABLE_NAME = "Vuelos";
	
	@DatabaseField(columnName = "Id", generatedId= true)
	private long id;
	@DatabaseField(columnName = "IdVuelo")
	private long idVuelo;
	@DatabaseField(columnName = "IdDocumento")
	private String idDocumento = "";
	@DatabaseField(columnName = "NumeroVuelo")
	private String numeroVuelo = "";
	@DatabaseField(columnName = "AerolineaCodigo")
	private String aerolineaCodigo = "";
	@DatabaseField(columnName = "AerolineaNombre")
	private String aerolineaNombre = "";
	@DatabaseField(columnName = "AerolineaTelefono")
	private String aerolineaTelefono = "";
	@DatabaseField(columnName = "AeropuertoOrigenCodigo")
	private String aeropuertoOrigenCodigo = "";
	@DatabaseField(columnName = "AeropuertoOrigenNombre")
	private String aeropuertoOrigenNombre = "";
	@DatabaseField(columnName = "AeropuertoOrigenDomicilio")
	private String aeropuertoOrigenDomicilio = "";
	@DatabaseField(columnName = "AeropuertoOrigenCiudad")
	private String aeropuertoOrigenCiudad = "";
	@DatabaseField(columnName = "AeropuertoOrigenPais")
	private String aeropuertoOrigenPais = "";
	@DatabaseField(columnName = "AeropuertoDestinoCodigo")
	private String aeropuertoDestinoCodigo = "";
	@DatabaseField(columnName = "AeropuertoDestinoNombre")
	private String aeropuertoDestinoNombre = "";
	@DatabaseField(columnName = "AeropuertoDestinoDomicilio")
	private String aeropuertoDestinoDomicilio = "";
	@DatabaseField(columnName = "AeropuertoDestinoCiudad")
	private String aeropuertoDestinoCiudad = "";
	@DatabaseField(columnName = "AeropuertoDestinoPais")
	private String aeropuertoDestinoPais = "";
	@DatabaseField(columnName = "AvionCodigo")
	private String avionCodigo = "";
	@DatabaseField(columnName = "AvionNombre")
	private String avionNombre = "";
	@DatabaseField(columnName = "Status")
	private String status = "";
	@DatabaseField(columnName = "StatusFecha")
	private String statusFecha = "";
	@DatabaseField(columnName = "FechaSalida")
	private String fechaSalida = "";
	@DatabaseField(columnName = "FechaLLegada")
	private String fechaLLegada = "";
	@DatabaseField(columnName = "ScheduledBlockMinutes")
	private int scheduledBlockMinutes;
	@DatabaseField(columnName = "BlockMinutes")
	private int blockMinutes;
	@DatabaseField(columnName = "ScheduledAirMinutes")
	private int scheduledAirMinutes;
	@DatabaseField(columnName = "AirMinutes")
	private int airMinutes;
	@DatabaseField(columnName = "ScheduledTaxiOutMinutes")
	private int scheduledTaxiOutMinutes;
	@DatabaseField(columnName = "TaxiOutMinutes")
	private int taxiOutMinutes;  
	@DatabaseField(columnName = "ScheduledTaxiInMinutes")
	private int scheduledTaxiInMinutes;
	@DatabaseField(columnName = "TaxiInMinutes")
	private int taxiInMinutes;
	
	  
    public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	public long getIdVuelo() {
		return idVuelo;
	}

	public void setIdVuelo(long idVuelo) {
		this.idVuelo = idVuelo;
	}

	public String getIdDocumento() {
		return idDocumento;
	}

	public void setIdDocumento(String idDocumento) {
		this.idDocumento = idDocumento;
	}

	public String getNumeroVuelo() {
		return numeroVuelo;
	}

	public void setNumeroVuelo(String numeroVuelo) {
		this.numeroVuelo = numeroVuelo;
	}

	public String getAerolineaCodigo() {
		return aerolineaCodigo;
	}

	public void setAerolineaCodigo(String aerolineaCodigo) {
		this.aerolineaCodigo = aerolineaCodigo;
	}

	public String getAerolineaNombre() {
		return aerolineaNombre;
	}

	public void setAerolineaNombre(String aerolineaNombre) {
		this.aerolineaNombre = aerolineaNombre;
	}

	public String getAerolineaTelefono() {
		return aerolineaTelefono;
	}

	public void setAerolineaTelefono(String aerolineaTelefono) {
		this.aerolineaTelefono = aerolineaTelefono;
	}

	public String getAeropuertoOrigenCodigo() {
		return aeropuertoOrigenCodigo;
	}

	public void setAeropuertoOrigenCodigo(String aeropuertoOrigenCodigo) {
		this.aeropuertoOrigenCodigo = aeropuertoOrigenCodigo;
	}

	public String getAeropuertoOrigenNombre() {
		return aeropuertoOrigenNombre;
	}

	public void setAeropuertoOrigenNombre(String aeropuertoOrigenNombre) {
		this.aeropuertoOrigenNombre = aeropuertoOrigenNombre;
	}

	public String getAeropuertoOrigenDomicilio() {
		return aeropuertoOrigenDomicilio;
	}

	public void setAeropuertoOrigenDomicilio(String aeropuertoOrigenDomicilio) {
		this.aeropuertoOrigenDomicilio = aeropuertoOrigenDomicilio;
	}

	public String getAeropuertoOrigenCiudad() {
		return aeropuertoOrigenCiudad;
	}

	public void setAeropuertoOrigenCiudad(String aeropuertoOrigenCiudad) {
		this.aeropuertoOrigenCiudad = aeropuertoOrigenCiudad;
	}

	public String getAeropuertoOrigenPais() {
		return aeropuertoOrigenPais;
	}

	public void setAeropuertoOrigenPais(String aeropuertoOrigenPais) {
		this.aeropuertoOrigenPais = aeropuertoOrigenPais;
	}

	public String getAeropuertoDestinoCodigo() {
		return aeropuertoDestinoCodigo;
	}

	public void setAeropuertoDestinoCodigo(String aeropuertoDestinoCodigo) {
		this.aeropuertoDestinoCodigo = aeropuertoDestinoCodigo;
	}

	public String getAeropuertoDestinoNombre() {
		return aeropuertoDestinoNombre;
	}

	public void setAeropuertoDestinoNombre(String aeropuertoDestinoNombre) {
		this.aeropuertoDestinoNombre = aeropuertoDestinoNombre;
	}

	public String getAeropuertoDestinoDomicilio() {
		return aeropuertoDestinoDomicilio;
	}

	public void setAeropuertoDestinoDomicilio(String aeropuertoDestinoDomicilio) {
		this.aeropuertoDestinoDomicilio = aeropuertoDestinoDomicilio;
	}

	public String getAeropuertoDestinoCiudad() {
		return aeropuertoDestinoCiudad;
	}

	public void setAeropuertoDestinoCiudad(String aeropuertoDestinoCiudad) {
		this.aeropuertoDestinoCiudad = aeropuertoDestinoCiudad;
	}

	public String getAeropuertoDestinoPais() {
		return aeropuertoDestinoPais;
	}

	public void setAeropuertoDestinoPais(String aeropuertoDestinoPais) {
		this.aeropuertoDestinoPais = aeropuertoDestinoPais;
	}

	public String getAvionCodigo() {
		return avionCodigo;
	}

	public void setAvionCodigo(String avionCodigo) {
		this.avionCodigo = avionCodigo;
	}

	public String getAvionNombre() {
		return avionNombre;
	}

	public void setAvionNombre(String avionNombre) {
		this.avionNombre = avionNombre;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getStatusText(Context context) {
		/* TODO REVISAR ESTO
		if (status.equalsIgnoreCase("A") || status.equalsIgnoreCase("DN") || status.equalsIgnoreCase("S"))
			return context.getString(R.string.flight_status_ontime);
		else if (status.equalsIgnoreCase("D") || status.equalsIgnoreCase("NO") || status.equalsIgnoreCase("R"))
			return context.getString(R.string.flight_status_delayed);
		else if (status.equalsIgnoreCase("L"))
			return context.getString(R.string.flight_status_landed);
		else if (status.equalsIgnoreCase("C"))
			return context.getString(R.string.flight_status_canceled);
		else
			return context.getString(R.string.flight_status_unknown);
			*/
		return null;
	}
	
	public String getStatusFecha() {
		return statusFecha;
	}

	public void setStatusFecha(String statusFecha) {
		this.statusFecha = statusFecha;
	}

	public String getFechaSalida() {
		return fechaSalida;
	}
	
	public GregorianCalendar getFechaSalidaCalendar() {
		//2014-06-02T22:00:00.000
		String[] fechaHora = fechaSalida.split("T");
		String[] fecha = fechaHora[0].split("-");
		String[] hora = fechaHora[1].split(":"); 
		GregorianCalendar calendar = new GregorianCalendar(
				Integer.parseInt(fecha[0]) ,
				(Integer.parseInt(fecha[1]) -1),
				Integer.parseInt(fecha[2]) ,
				Integer.parseInt(hora[0]) ,
				Integer.parseInt(hora[1])
				);
		return calendar;
	}
	
	public void setFechaSalida(String fechaSalida) {
		this.fechaSalida = fechaSalida;
	}

	public String getFechaLLegada() {
		return fechaLLegada;
	}
	public GregorianCalendar getFechaLlegadaCalendar() {
		//2014-06-02T22:00:00.000
		String[] fechaHora = fechaLLegada.split("T");
		String[] fecha = fechaHora[0].split("-");
		String[] hora = fechaHora[1].split(":"); 
		GregorianCalendar calendar = new GregorianCalendar(
				Integer.parseInt(fecha[0]) ,
				(Integer.parseInt(fecha[1]) -1),
				Integer.parseInt(fecha[2]) ,
				Integer.parseInt(hora[0]) ,
				Integer.parseInt(hora[1])
				);
		return calendar;
	}
	public void setFechaLLegada(String fechaLLegada) {
		this.fechaLLegada = fechaLLegada;
	}

	public int getScheduledBlockMinutes() {
		return scheduledBlockMinutes;
	}

	public void setScheduledBlockMinutes(int scheduledBlockMinutes) {
		this.scheduledBlockMinutes = scheduledBlockMinutes;
	}

	public int getBlockMinutes() {
		return blockMinutes;
	}

	public void setBlockMinutes(int blockMinutes) {
		this.blockMinutes = blockMinutes;
	}

	public int getScheduledAirMinutes() {
		return scheduledAirMinutes;
	}

	public void setScheduledAirMinutes(int scheduledAirMinutes) {
		this.scheduledAirMinutes = scheduledAirMinutes;
	}

	public int getAirMinutes() {
		return airMinutes;
	}

	public void setAirMinutes(int airMinutes) {
		this.airMinutes = airMinutes;
	}

	public int getScheduledTaxiOutMinutes() {
		return scheduledTaxiOutMinutes;
	}

	public void setScheduledTaxiOutMinutes(int scheduledTaxiOutMinutes) {
		this.scheduledTaxiOutMinutes = scheduledTaxiOutMinutes;
	}

	public int getTaxiOutMinutes() {
		return taxiOutMinutes;
	}

	public void setTaxiOutMinutes(int taxiOutMinutes) {
		this.taxiOutMinutes = taxiOutMinutes;
	}

	public int getScheduledTaxiInMinutes() {
		return scheduledTaxiInMinutes;
	}

	public void setScheduledTaxiInMinutes(int scheduledTaxiInMinutes) {
		this.scheduledTaxiInMinutes = scheduledTaxiInMinutes;
	}

	public int getTaxiInMinutes() {
		return taxiInMinutes;
	}

	public void setTaxiInMinutes(int taxiInMinutes) {
		this.taxiInMinutes = taxiInMinutes;
	}

	private static final String TABLE_CREATE = "CREATE TABLE "
	    + TABLE_NAME +
    	" (Id                         INTEGER  PRIMARY KEY AUTOINCREMENT,"+
    	"  IdVuelo                    NUMERIC( 10 ),"+
    	"  IdDocumento                TEXT,"+
    	"  NumeroVuelo                TEXT,"+
    	"  AerolineaCodigo            CHAR( 20 ),"+
    	"  AerolineaNombre            TEXT,"+
    	"  AerolineaTelefono          TEXT,"+
    	"  AeropuertoOrigenCodigo     CHAR( 20 ),"+
    	"  AeropuertoOrigenNombre     TEXT,"+
    	"  AeropuertoOrigenDomicilio  TEXT,"+
    	"  AeropuertoOrigenCiudad     TEXT,"+
    	"  AeropuertoOrigenPais       TEXT,"+
    	"  AeropuertoDestinoCodigo    CHAR( 20 ),"+
    	"  AeropuertoDestinoNombre    TEXT,"+
    	"  AeropuertoDestinoDomicilio TEXT,"+
    	"  AeropuertoDestinoCiudad    TEXT,"+
    	"  AeropuertoDestinoPais      TEXT,"+
    	"  AvionCodigo                CHAR( 20 ),"+
    	"  AvionNombre                TEXT,"+
    	"  Status                     CHAR( 5 ),"+
    	"  StatusFecha                CHAR( 20 ),"+
    	"  FechaSalida                TEXT,"+
    	"  FechaLLegada               TEXT,"+
    	"  ScheduledBlockMinutes      INT,"+
    	"  BlockMinutes               INT,"+
    	"  ScheduledAirMinutes        INT,"+
    	"  AirMinutes                 INT,"+
    	"  ScheduledTaxiOutMinutes    INT,"+
    	"  TaxiOutMinutes             INT,"+
    	"  ScheduledTaxiInMinutes     INT,"+
    	"  TaxiInMinutes              INT)";
    
    public static void onCreate(SQLiteDatabase database) {
    	database.execSQL(TABLE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,int newVersion) {
		database.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
		onCreate(database);
    }
}
