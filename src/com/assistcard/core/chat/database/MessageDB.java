package com.assistcard.core.chat.database;

import android.database.sqlite.SQLiteDatabase;
import android.text.Html;
import android.text.Spanned;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "Chat")
public class MessageDB {			
	public static final String TABLE_NAME = "Chat";
		
	@DatabaseField(columnName = "Id", generatedId= true)
	private long id;
	@DatabaseField(columnName = "DocumentNumber")
	private String DocumentNumber;
	@DatabaseField(columnName = "Operador")
	private Boolean operador;
	@DatabaseField(columnName = "Mensaje")
	private String mensaje;
	@DatabaseField(columnName = "Fecha")
	private long fecha;
	@DatabaseField(columnName = "Nombre")
	private String nombre;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getDocumentNumber() {
		return DocumentNumber;
	}
	public void setDocumentNumber(String documentNumber) {
		DocumentNumber = documentNumber;
	}
	public Boolean getOperador() {
		return operador;
	}
	public void setOperador(Boolean operador) {
		this.operador = operador;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public long getFecha() {
		return fecha;
	}
	public void setFecha(long fecha) {
		this.fecha = fecha;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String pNombre) {
		this.nombre = pNombre;
	}
	
	public static final String TABLE_CREATE = "CREATE TABLE Chat (" +
			"Id             INTEGER        PRIMARY KEY," +
			"DocumentNumber TEXT," +
			"Operador       BOOLEAN," +
			"Mensaje        TEXT," +
			"Nombre        TEXT," +
			"Fecha          NUMERIC( 14 ));";
	
	public static void onCreate(SQLiteDatabase database) {
    	database.execSQL(TABLE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,int newVersion) {
		database.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
		onCreate(database);
    }
	
    public boolean containsUrl(){
    	if(mensaje==null || mensaje.length() == 0)
    		return false;
    	return mensaje.contains("<a") && mensaje.contains("</a");
    }
    
    public String getUrl(){
    	if(mensaje==null || mensaje.length() == 0)
    		return "";
    	if(mensaje.contains("href=''"))
    		return getUrlVisible();
    	else
    		return getUrlHref();
    }
    
    private String getUrlVisible(){
    	if(mensaje==null || mensaje.length() == 0)
    		return "";
    	int i = mensaje.indexOf("<a");
    	String urlTmp = "";
    	if (i>-1) {
    		urlTmp = mensaje.substring(i);
    		urlTmp = urlTmp.substring(urlTmp.indexOf(">"));
    		urlTmp = urlTmp.substring(1, urlTmp.indexOf("</"));
		}
    	return urlTmp;
    }
    private String getUrlHref(){
    	int i = mensaje.indexOf("href='")+6;
    	String url = mensaje.substring(i);
    	url = url.substring(0, url.indexOf("'"));
    	return url;
    }
    
    public Spanned getMessageWithUrl(){
    	return Html.fromHtml(mensaje!=null ? mensaje : "");
    	/*try {
    		int i = mensaje.indexOf("<a");
        	String msgTmp = mensaje.substring(0, i+1) + "u";
        	i = mensaje.indexOf("</a");
        	msgTmp = msgTmp + mensaje.substring(mensaje.indexOf(">"),i);
        	msgTmp = msgTmp + "</u>" + mensaje.substring(i+4);
        	return Html.fromHtml(msgTmp);
		} catch (Exception e) {
			return Html.fromHtml(mensaje);
		}*/
    	
    }
}
