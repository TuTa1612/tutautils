package com.assistcard.core.chat.database;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import android.text.Html;
import android.util.Log;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;

public class ChatDAO {
	/*
	public static void insertAsync(Context ctx, DatabaseHelper databaseHelper ,String documentNumber ,List<MessageJSON> mensajes, ChatDAOListener listener){
		InsertTask task = new InsertTask(ctx, databaseHelper, documentNumber, mensajes);
		task.execute(listener);
	}
	
	public static class InsertTask extends AsyncTask<ChatDAOListener, Void, List<MessageDB>>{
		ChatDAOListener mListener;
		private Context mContext;
		public DatabaseHelper mDatabaseHelper;
		public String mDocumentNumber;
		//public List<MessageJSON> mMessagesJson;
		
		public InsertTask(Context ctx, DatabaseHelper databaseHelper ,String documentNumber ,List<MessageJSON> mensajes) {
			mContext = ctx;
			mDatabaseHelper = databaseHelper;
			mDocumentNumber = documentNumber;
			//mMessagesJson = mensajes;
		}
		@Override
		protected List<MessageDB> doInBackground(ChatDAOListener... params) {
			mListener = params[0];
			return insert(mContext, mDatabaseHelper, mDocumentNumber, mMessagesJson);
		}
		 @Override
		protected void onPostExecute(List<MessageDB> result) {
			mListener.onDAOResult(result);
		}
	}*/
	/**
	 * 
	 * @param databaseHelper
	 * @param documentNumber
	 * @param mensajes
	 * @return Devuelve solo los mensajes nuevos:
	 */
	/*private static List<MessageDB> insert(Context ctx, DatabaseHelper databaseHelper ,String documentNumber ,List<MessageJSON> mensajes) {
		List<MessageDB> listNew = new ArrayList<MessageDB>();
		try {
			Dao<MessageDB, Long> dao = databaseHelper.getChatDao();
			for (MessageJSON mensaje : mensajes){
				String fechaString = mensaje.getDate().replace("Date(", "");///Date(1405529164790)/
				fechaString = fechaString.replace("/", "");
				fechaString = fechaString.replace(")", "");
				try{
					if (fechaString.length() > 0){
						long fecha = Long.parseLong(fechaString);
						if (!controlaExiste(databaseHelper ,documentNumber ,fecha, mensaje.getMessage())){
							MessageDB chat = new MessageDB();
							chat.setDocumentNumber(documentNumber);
							chat.setFecha(fecha);
							chat.setMensaje(mensaje.getMessage());
							if (mensaje.getUser() == null || mensaje.getUser().getUserType().equalsIgnoreCase("Operator")){
								chat.setOperador(true);
								String nombre = mensaje.getUser()!=null&&mensaje.getUser().getName()!=null ? mensaje.getUser().getName() :"System";
								String apellido = mensaje.getUser()!=null&&mensaje.getUser().getLastName()!=null ? mensaje.getUser().getLastName() :"";
								chat.setNombre(String.format("%s %s", nombre, apellido));
							}else{
								chat.setOperador(false);
								chat.setNombre(PrefsHelper.with(ctx).getVoucher().getNombre());
							}
							
							dao.create(chat);
							listNew.add(chat);
						}
					}
				}catch(Exception e){
					Log.e(DatabaseHelper.TAG, e.toString());
				}
			}
			
			/*QueryBuilder<MessageDB, Long> queryBuilder = dao.queryBuilder();
			queryBuilder.where().eq("DocumentNumber", documentNumber);
			queryBuilder.orderBy("Fecha", true);
			list = dao.query(queryBuilder.prepare());/
		} catch (SQLException e) {
			Log.e(DatabaseHelper.TAG, e.toString());
		}
		return listNew;
	}*/
	
	
	/**
	 * 
	 * @param databaseHelper
	 * @param documentNumber
	 * @return Devuelve todos los mensajes guardados
	 */
	public static List<MessageDB> queryAll(DatabaseHelper databaseHelper ,String documentNumber) {
		List<MessageDB> list = new ArrayList<MessageDB>();
		try {
			Dao<MessageDB, Long> dao = databaseHelper.getChatDao();
			QueryBuilder<MessageDB, Long> queryBuilder = dao.queryBuilder();
			queryBuilder.where().eq("DocumentNumber", documentNumber);
			queryBuilder.orderBy("Fecha", true);
			list = dao.query(queryBuilder.prepare());
		} catch (SQLException e) {
			Log.e(DatabaseHelper.TAG, e.toString());
		}
		return list;
	}
	
	private static boolean controlaExiste(DatabaseHelper databaseHelper, String documentNumber, long fecha, String mensaje) {
		boolean existe = false;
		try {
			Dao<MessageDB, Long> dao = databaseHelper.getChatDao();
			QueryBuilder<MessageDB, Long> queryBuilder = dao.queryBuilder();
			queryBuilder.where()
				.eq("DocumentNumber", documentNumber)
				.and()
				.between("Fecha", fecha-3000, fecha+3000)
				.and()
				.eq("Mensaje", mensaje.replace("'", "\'"));
			if (dao.query(queryBuilder.prepare()).size() > 0)
				existe = true;
		} catch (SQLException e) {
			existe = findLike(databaseHelper, documentNumber, fecha, mensaje);
		}
		return existe;
	}
	
	private static boolean findLike(DatabaseHelper databaseHelper, String documentNumber, long fecha, String mensaje){
		boolean existe = false;
		try {
			Dao<MessageDB, Long> dao2 = databaseHelper.getChatDao();
			QueryBuilder<MessageDB, Long> queryBuilder2 = dao2.queryBuilder();
			String likeValue = "%"+Html.fromHtml(mensaje).toString().trim()+"%";
			queryBuilder2.where()
				.eq("DocumentNumber", documentNumber)
				.and()
				.between("Fecha", fecha-3000, fecha+3000)
				.and()
				.like("Mensaje", likeValue);
			if(dao2.query(queryBuilder2.prepare()).size()>0)
				existe = true;
		} catch (SQLException e) {
			existe = true;
		}
		return existe;
	}
}
