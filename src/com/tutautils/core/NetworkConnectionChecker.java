package com.tutautils.core;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;

public class NetworkConnectionChecker {

	public static AlertDialog showGPSDisabledAlertToUser(final Context ctx,
			String message, String buttonText) {
		return showDisabledAlertToUser(ctx, message, buttonText,
				Settings.ACTION_LOCATION_SOURCE_SETTINGS);
	}

	private static AlertDialog showDisabledAlertToUser(final Context ctx,
			String message, String buttonText, final String sourceSettings) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctx);
		alertDialogBuilder
				.setMessage(message)
				.setCancelable(false)
				.setPositiveButton(buttonText,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int id) {
								Intent intent = new Intent(sourceSettings);
								ctx.startActivity(intent);
								dialog.dismiss();
							}
						});
		return alertDialogBuilder.create();
	}

	public static boolean hasInternetConnection(final Context ctx) {
		ConnectivityManager cm = (ConnectivityManager) ctx
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = cm.getActiveNetworkInfo();
		if (activeNetworkInfo == null) {
			return false;
		}
		return activeNetworkInfo.isConnectedOrConnecting();
	}

}
