package com.tutautils.core;

public interface ILoading {
	public void showLoading(boolean cancelable, String mensaje, String cancelMessage);
	public void showLoading(boolean cancelable, String mensaje);
	public void showLoading(boolean cancelable);
	public void hideLoading(String mensaje, boolean lengthLong);
	public void hideLoading();
}
