package com.tutautils.core;

public interface GPSListener{
	public void PedirGPS();
	public void ActualizarUbicacion(float latitud, float longitud);
}
