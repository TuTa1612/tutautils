package com.tutautils.core;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
//import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.model.LatLng;

/**
 * @author lucas.saavedra
 * requiere los siguientes permisos 
 * <uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION"/>
 * <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION"/>
 * <uses-permission android:name="android.permission.INTERNET"/>
 */
public class GPSHelperOLD implements GooglePlayServicesClient.ConnectionCallbacks, GooglePlayServicesClient.OnConnectionFailedListener{
	private Context mContext;
	private GPSListener mListener;
	//LocationClient mLocationClientPLAY;
	LocationManager locationManagerOLD;
	private SharedPreferences mSharedPreferences;
	private final static String LAST_LOCATION = "LastLocation";
	private long refresh = 0;
	private boolean notificarGPSSiempre= false;
	com.google.android.gms.location.LocationListener listenerLocationPLAY;
	android.location.LocationListener listenerLocationOLD;
	
	public GPSHelperOLD(Context ctx, GPSListener activity, long tiempoDeRefresco, boolean notificarSiempre){
		mContext = ctx;
		mListener = activity;
		refresh = tiempoDeRefresco;
		notificarGPSSiempre = notificarSiempre;
		mSharedPreferences = mContext.getSharedPreferences(LAST_LOCATION, Context.MODE_PRIVATE);
		
		if(servicesConnected()){
			   //mLocationClientPLAY = new LocationClient(mContext, this, this);
			   //mLocationClientPLAY.connect();
			   
			   listenerLocationPLAY = new com.google.android.gms.location.LocationListener() {
				@Override
				public void onLocationChanged(Location location) {
					if(getLastLAT() != (float)location.getLatitude() || getLastLON() != (float)location.getLongitude() || notificarGPSSiempre){
						saveLastLocation((float)location.getLatitude(), (float) location.getLongitude());
						mListener.ActualizarUbicacion(getLastLAT(), getLastLON());
					}
				}
			};
		   }
		else{
			String provider = Settings.Secure.getString(mContext.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        	if(provider == null || provider.trim().length() == 0 || !provider.contains("gps")){
        		mListener.PedirGPS();
        	}
			locationManagerOLD = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
			listenerLocationOLD = new android.location.LocationListener() {
				public void onStatusChanged(String provider, int status, Bundle extras) {}
				public void onProviderEnabled(String provider) {}
				public void onProviderDisabled(String provider) {}
				public void onLocationChanged(Location location) {
					if (location != null) {
						if(refresh == 0){
							locationManagerOLD.removeUpdates(this);
						}
						if(getLastLAT() != (float)location.getLatitude() || getLastLON() != (float)location.getLongitude() || notificarGPSSiempre){
							saveLastLocation((float)location.getLatitude(), (float) location.getLongitude());
							mListener.ActualizarUbicacion(getLastLAT(), getLastLON());
						}
					}
				}
			};
	        locationManagerOLD.requestLocationUpdates(LocationManager.GPS_PROVIDER, refresh, 0, listenerLocationOLD);
		}
	}
	
	
	
	private boolean servicesConnected() {
        // Check that Google Play services is available
		// If Google Play services is available returns true
		try{
			int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(mContext);
        	return ConnectionResult.SUCCESS == resultCode;
        }catch(Exception e){
        	return false;
        }
    }
	
	/*
     * Called by Location Services when the request to connect the
     * client finishes successfully. At this point, you can
     * request the current location or start periodic updates
     */
	@Override
    public void onConnected(Bundle bundle) {
		/*Location currentLocation = mLocationClientPLAY.getLastLocation();
		if (currentLocation != null) {
			saveLastLocation((float)currentLocation.getLatitude(), (float)currentLocation.getLongitude());
			mListener.ActualizarUbicacion(getLastLAT(), getLastLON());
			if(refresh > 0){
				LocationRequest locationRequest = LocationRequest.create();
				locationRequest.setInterval(refresh);
				locationRequest.setFastestInterval((long)(refresh*0.8));
				mLocationClientPLAY.requestLocationUpdates(locationRequest, listenerLocationPLAY);
			}else{
				mLocationClientPLAY.disconnect();
			}
		}*/
    }
    
    

    /*
     * Called by Location Services if the connection to the
     * location client drops because of an error.
     */
	@Override
    public void onDisconnected() {
		Log.d("GPSHelper", "LocationClient Disconnected");
	}

    /*
     * Called by Location Services if the attempt to
     * Location Services fails.
     */
	@Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
		Log.d("GPSHelper", "LocationClient onConnectionFailed whit ErrorCode:"+String.valueOf(connectionResult.getErrorCode()));
    }
	
	
	public void saveLastLocation(float lat, float lon){
		SharedPreferences.Editor mEditor = mSharedPreferences.edit();
		mEditor.putFloat("LastLAT", lat);
		mEditor.putFloat("LastLON", lon);
		mEditor.commit();
	}
	
	public float getLastLAT(){
		return mSharedPreferences.getFloat("LastLAT", 0);
	}
	
	public float getLastLON(){
		return mSharedPreferences.getFloat("LastLON", 0);
	}
	
	public void StopGPS(){
		if(locationManagerOLD != null)
		{
			locationManagerOLD.removeUpdates(listenerLocationOLD);
		}
		/*if(mLocationClientPLAY != null){
			mLocationClientPLAY.disconnect();
		}*/
	}
	
	public static LatLng GetLastPosition(Context ctx){
		SharedPreferences pref = ctx.getSharedPreferences(LAST_LOCATION, Context.MODE_PRIVATE);
		double lat = pref.getFloat("LastLAT", 0);
		double lon = pref.getFloat("LastLON", 0);
		pref = null;
		
		LatLng point = new LatLng(lat, lon);
		return point;
	}
}
