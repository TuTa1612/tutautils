package com.tutautils.core;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

public abstract class OnFlingGestureListener implements OnTouchListener {
	private float oldX, oldY;
	private boolean move;
	private final int SWIPE_MIN_DISTANCE = 100;

	@Override
	public boolean onTouch(final View v, final MotionEvent event) {
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			oldX = event.getX();
			oldY = event.getY();
			move = false;
		}
		if (event.getAction() == MotionEvent.ACTION_MOVE) {
			move = true;
		}
		if (event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL) {
			if (move) {
				move = false;
				return myGesture(oldX, oldY, event.getX(), event.getY());
			}
		}
		
		return false;
	}

	
	private boolean myGesture(float oldX, float oldY, float newX, float newY){
		float difX = newX - oldX;
		float difY = newY - oldY;
		
		if(Math.abs(difX) < SWIPE_MIN_DISTANCE && Math.abs(difY) <SWIPE_MIN_DISTANCE)
			return false;
		
		if(Math.abs(difY) > Math.abs(difX)){
				if (difY < 0) {
					onTopToBottom();
					return true;
				} else {
					onBottomToTop();
					return true;
				}
		}else{

				if (difX < 0) {
					onRightToLeft();
					return true;
				} else {
					onLeftToRight();
					return true;
				}
		}
	}
	

	public abstract void onRightToLeft();

	public abstract void onLeftToRight();

	public abstract void onBottomToTop();

	public abstract void onTopToBottom();

}