package com.tutautils.core;

import java.util.Date;
import java.util.Locale;
import java.util.regex.Pattern;

import android.content.Context;
import android.content.res.Configuration;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

public class Utils {
	
	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

	private static Pattern EMAIL_ADDRESS_PATTERN = Pattern
			.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
	
	public static boolean isValidEmail(String email) {
		return EMAIL_ADDRESS_PATTERN.matcher(email).matches();
	}
	
	public static boolean isNullOrEmpty(String var) {
		if (var == null || var == "" || var.length() == 0
				|| var.trim().length() == 0)
			return true;
		return false;
	}
	
	public static boolean isValidEditText(EditText var) {
		if (var == null || var.getText().toString() == "" || 
				var.getText().toString().length() == 0
				|| var.getText().toString().trim().length() == 0)
			return false;
		return true;
	}
	
	public static boolean isValidButtonValue(Button var) {
		if (var == null || var.getText()==null || var.getText().toString() == "" || 
				var.getText().toString().length() == 0
				|| var.getText().toString().trim().length() == 0)
			return false;
		return true;
	}
	
	public static boolean isValidButtonTag(Button var) {
		if (var == null || var.getTag() ==null || var.getTag().toString() == ""
				|| var.getText().toString().trim().length() == 0)
			return false;
		return true;
	}
	
	public static int StringToNumber(String texto) {
		try {
			return Integer.parseInt(texto);
		} catch (Exception e) {
			return 0;
		}
	}
	
	/**
	 * 
	 * @param fecha el formato valido es YYYY-MM-DD o YYYY/MM/DD
	 * @return
	 */
	public static Date ConvertDate(String fecha) {
		int year = Integer.parseInt(fecha.substring(0, 4)) - 1900;
		int month = Integer.parseInt(fecha.substring(5, 7)) - 1;
		int day = Integer.parseInt(fecha.substring(8));
		Date _date = new Date(year, month, day);
		return _date;
	}

	public static String DateToStringDDMMYYYY(Date unaFecha, char separador) {
		String stringFecha = new String();
		stringFecha = stringFecha + String.format("%02d", unaFecha.getDate()) + separador;
		stringFecha = stringFecha + String.format("%02d", unaFecha.getMonth() + 1) + separador;
		stringFecha = stringFecha + String.valueOf(unaFecha.getYear() + 1900);
		return stringFecha;
	}

	public static String DateToStringYYYYMMDD(Date unaFecha, char separador) {
		String stringFecha = new String();
		stringFecha = stringFecha + String.valueOf(unaFecha.getYear() + 1900) + separador;
		stringFecha = stringFecha + String.format("%02d", unaFecha.getMonth() + 1) + separador;
		stringFecha = stringFecha + String.format("%02d", unaFecha.getDate());

		return stringFecha;
	}
	
	public static String DateStringYYYYMMDDtoDDMMYYYY(String yyyyMMdd, char separador){
		String stringFecha = new String();
		stringFecha = stringFecha + String.format("%s", yyyyMMdd.substring(8, 10)) + separador;
		stringFecha = stringFecha + String.format("%s", yyyyMMdd.substring(5, 7)) + separador;
		stringFecha = stringFecha + String.format("%s", yyyyMMdd.substring(0, 4));
		return stringFecha;
	}
	
	public static boolean changeLanguage(Context ctx, String language) {
			Locale locale = new Locale(language);
			Locale.setDefault(locale);
			Configuration config = new Configuration();
			config.locale = locale;
			ctx.getResources().updateConfiguration(config, ctx.getResources().getDisplayMetrics());
			
			return true;
	}
	
	public static boolean CheckPlayServices(Context ctx) {
	    int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(ctx);
	    if (resultCode != ConnectionResult.SUCCESS) {
//	        if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
//	            //GooglePlayServicesUtil.getErrorDialog(resultCode, this, PLAY_SERVICES_RESOLUTION_REQUEST).show();
//	        } else {
//	            //Log.i(TAG, "This device is not supported.");
//	            //finish();
//	        	return false;
//	        }
	        return false;
	    }
	    return true;
	}

}
