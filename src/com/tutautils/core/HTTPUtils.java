package com.tutautils.core;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.entity.mime.MultipartEntity;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

/**
 * @author lucas.saavedra
 * requiere los siguientes permisos 
 * <uses-permission android:name="android.permission.INTERNET"/>
 */
public class HTTPUtils {
	public HTTPUtils(){}
	
	///GET con parametros via URL
	public void CallWSGetWithURLParamsAsync(String url, HashMap<String, Object> params, OnHTTPutilsResultListener listener){
		try {
			Class.forName("android.os.AsyncTask");
			new AsyncRequestGET().execute(new URLParamsYListener(url, params, listener));
		} catch (ClassNotFoundException e) {
			new MyAsyncRequestGET(new URLParamsYListener(url, params, listener)).execute();
		}
	}
	
	protected class MyAsyncRequestGET{
		@SuppressLint("HandlerLeak")
		private Handler mHandler = new Handler() {
		    @Override
		    public void handleMessage(Message msg) {
		        if (msg.obj!=null) {
					Respuesta rta = (Respuesta) msg.obj;
					postExecute(rta, mUrlParamsyListener.getListener());
				}
		    }
		};
		 
		Runnable mRunnable = new Runnable() {
		    public void run() {
				boolean status;
				String resultado = null;
				try {
					status = true;
					resultado = CallWSGetWithURLParams(mUrlParamsyListener.getUrl(), mUrlParamsyListener.getParams());
				} catch (Exception e) {
					status = false;
					resultado = e.getStackTrace().toString().toString() + "\nurl: " + mUrlParamsyListener.url 
							+ "\nparams:" +(mUrlParamsyListener.getParams()!=null ? mUrlParamsyListener.getParams().toString() : "null");
				}
				Message msgRta = new Message();
				msgRta.obj = new Respuesta(status, resultado);
				mHandler.sendMessage(msgRta);
		    }
		};
		
		URLParamsYListener mUrlParamsyListener;
		public MyAsyncRequestGET(URLParamsYListener urlParamsyListener){
			mUrlParamsyListener = urlParamsyListener;
		}
		public void execute(){
			Thread mThread = new Thread(mRunnable);
			mThread.start();
		}
	}
	
	protected class AsyncRequestGET extends AsyncTask<URLParamsYListener, Integer, Respuesta>{
		OnHTTPutilsResultListener handler;
		
		@Override
		protected Respuesta doInBackground(URLParamsYListener... params) {
			URLParamsYListener urlandparams = params[0];
			boolean status;
			String resultado = null;
			handler = urlandparams.getListener();
			try {
				status = true;
				resultado = CallWSGetWithURLParams(urlandparams.getUrl(), urlandparams.getParams());
			} catch (Exception e) {
				status = false;
				resultado = e.getStackTrace().toString().toString() + "\nurl: " + params[0].url 
						+ "\nparams:" +(params!=null ? params.toString() : "null");
			}
			
			Respuesta rta = new Respuesta(status, resultado);
			return rta;
		}
				
		@Override
		protected void onPostExecute(Respuesta respuesta) {
			postExecute(respuesta, handler);
		}
	}
	
	private String CallWSGetWithURLParams(String url, HashMap<String, Object> params) throws ClientProtocolException, IOException{
		String urlFinal = addParamsToURL(url, params);
		HttpResponse response;
		HttpClient client = new DefaultHttpClient();
		HttpGet get = new HttpGet(urlFinal);
		
		response = client.execute(get);
		
		String result = null;
		HttpEntity httpentity = response.getEntity();
		if (httpentity != null) {
			InputStream instream = httpentity.getContent();
			result = convertStreamToString(instream);
			instream.close();
		}
		Loggear(urlFinal, params, result);
		return result;
	}
	
	///POST con parametros via URL
	public void CallWSPostWithURLParamsAsync(String url, HashMap<String, Object> params, OnHTTPutilsResultListener listener){
		new AsyncRequestPOSTUrlParams().execute(new URLParamsYListener(url, params, listener));
	}
	
	protected class AsyncRequestPOSTUrlParams extends AsyncTask<URLParamsYListener, Integer, Respuesta>{
		OnHTTPutilsResultListener handler;
		
		@Override
		protected Respuesta doInBackground(URLParamsYListener... params) {
			URLParamsYListener urlandparams = params[0];
			boolean status;
			String resultado = null;
			handler = urlandparams.getListener();
			try {
				status = true;
				resultado = CallWSPostWithURLParams(urlandparams.getUrl(), urlandparams.getParams());
			} catch (Exception e) {
				status = false;
				resultado = e.getStackTrace().toString().toString() + "\nurl: " + params[0].url 
						+ "\nparams:" +(params!=null ? params.toString() : "null");
			}
			
			Respuesta rta = new Respuesta(status, resultado);
			return rta;
		}
				
		@Override
		protected void onPostExecute(Respuesta respuesta) {
			postExecute(respuesta, handler);
		}
	}
	private String CallWSPostWithURLParams(String url, HashMap<String, Object> params) throws ClientProtocolException, IOException{
		String urlFinal = addParamsToURL(url, params);
		HttpResponse response;
		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(urlFinal);
		
		response = client.execute(post);
		
		String result = null;
		HttpEntity httpentity = response.getEntity();
		if (httpentity != null) {
			InputStream instream = httpentity.getContent();
			result = convertStreamToString(instream);
			instream.close();
		}
		Loggear(urlFinal, params, result);
		return result;
	}
	
	
	///POST con parametros via X WWW FORM
	public void CallWSPostWithFormParamsAsync(String url, HashMap<String, Object> params, OnHTTPutilsResultListener listener){
		try {
			Class.forName("android.os.AsyncTask");
			new AsyncRequestPOSTFormParams().execute(new URLParamsYListener(url, params, listener));
		} catch (ClassNotFoundException e) {
			new MyAsyncRequestPOSTWithFormParams(new URLParamsYListener(url, params, listener)).execute();
		}
	}
	
	protected class MyAsyncRequestPOSTWithFormParams{
		@SuppressLint("HandlerLeak")
		private Handler mHandler = new Handler() {
		    @Override
		    public void handleMessage(Message msg) {
		        if (msg.obj!=null) {
					Respuesta rta = (Respuesta) msg.obj;
					postExecute(rta, mUrlParamsyListener.getListener());
				}
		    }
		};
		 
		Runnable mRunnable = new Runnable() {
		    public void run() {
				boolean status;
				String resultado = null;
				try {
					status = true;
					resultado = CallWSPostWithFormParams(mUrlParamsyListener.getUrl(), mUrlParamsyListener.getParams());
				} catch (Exception e) {
					status = false;
					resultado = e.getStackTrace().toString().toString() + "\nurl: " + mUrlParamsyListener.url 
							+ "\nparams:" +(mUrlParamsyListener.getParams()!=null ? mUrlParamsyListener.getParams().toString() : "null");
				}
				
				Message msgRta = new Message();
				msgRta.obj = new Respuesta(status, resultado);
				mHandler.sendMessage(msgRta);
		    }
		};
		
		URLParamsYListener mUrlParamsyListener;
		public MyAsyncRequestPOSTWithFormParams(URLParamsYListener urlParamsyListener){
			mUrlParamsyListener = urlParamsyListener;
		}
		public void execute(){
			Thread mThread = new Thread(mRunnable);
			mThread.start();
		}
	}
	
	protected class AsyncRequestPOSTFormParams extends AsyncTask<URLParamsYListener, Integer, Respuesta>{
		OnHTTPutilsResultListener handler;
		
		@Override
		protected Respuesta doInBackground(URLParamsYListener... params) {
			URLParamsYListener urlandparams = params[0];
			boolean status;
			String resultado = null;
			handler = urlandparams.getListener();
			try {
				status = true;
				resultado = CallWSPostWithFormParams(urlandparams.getUrl(), urlandparams.getParams());
			} catch (Exception e) {
				status = false;
				resultado = e.getStackTrace().toString().toString() + "\nurl: " + params[0].url 
						+ "\nparams:" +(params!=null ? params.toString() : "null");
			}
			
			Respuesta rta = new Respuesta(status, resultado);
			return rta;
		}
				
		@Override
		protected void onPostExecute(Respuesta respuesta) {
			postExecute(respuesta, handler);
		}
	}
	
	private String CallWSPostWithFormParams(String url, HashMap<String, Object> params) throws ClientProtocolException, IOException{
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(url);
        httppost.setHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
        
        List<NameValuePair> formParams = new ArrayList<NameValuePair>();
        for (String k : params.keySet()) {
			formParams.add(new BasicNameValuePair(k, String.valueOf(params.get(k))));
		}
        httppost.setEntity(new UrlEncodedFormEntity(formParams, "UTF-8"));

        // Execute HTTP Post Request
        HttpResponse response = httpclient.execute(httppost);
        String result = null;
		HttpEntity httpentity = response.getEntity();
		if (httpentity != null) {
			InputStream instream = httpentity.getContent();
			result = convertStreamToString(instream);
			instream.close();
		}
		Loggear(url, params, result);
		return result;
	}
	
	
	public void CallWSPostWithURLParamsAndMultipartParams(String url, HashMap<String, Object> urlParams, MultipartEntity multipartParams, OnHTTPutilsResultListener listener){
		new AsyncPOSTWithURLParamsAndMultipartParams().execute(new URLParamsMultipartYlistener(url, urlParams, multipartParams, listener));
	}
	
	protected class AsyncPOSTWithURLParamsAndMultipartParams extends AsyncTask<URLParamsMultipartYlistener, Integer, Respuesta>{
		OnHTTPutilsResultListener listener;
		
		@Override
		protected Respuesta doInBackground(URLParamsMultipartYlistener... params) {
			URLParamsMultipartYlistener urlandparams = params[0];
			boolean status;
			String resultado = null;
			listener = urlandparams.getListener();
			try {
				status = true;
				resultado = CallWSPostWithURLParamsAndMultipartParams(urlandparams.getUrl(), urlandparams.getParams(), urlandparams.getMultipartParams());
			} catch (Exception e) {
				status = false;
				resultado = e.getStackTrace().toString().toString() + "\nurl: " + params[0].getUrl() 
						+ "\nparams:" +(params!=null ? params.toString() : "null");
			}
			
			Respuesta rta = new Respuesta(status, resultado);
			return rta;
		}
				
		@Override
		protected void onPostExecute(Respuesta rta) {
			if(rta.getStatus()){
				listener.OnResult(rta.getResultado());
			}else{
				listener.OnFailed(rta.getResultado());
			}
		}
	}
	
	private String CallWSPostWithURLParamsAndMultipartParams(String urlOriginal, HashMap<String, Object> urlParams, MultipartEntity multipartParams) throws ClientProtocolException, IOException{
		HttpClient httpclient = new DefaultHttpClient();
		String urlFinal = addParamsToURL(urlOriginal, urlParams);
		HttpPost httppost = new HttpPost(urlFinal);
		//httppost.setHeader("Content-Type", "multipart/form-data");
		httppost.setEntity(multipartParams);

        HttpResponse response = httpclient.execute(httppost);
        String result = null;
		HttpEntity httpentity = response.getEntity();
		if (httpentity != null) {
			InputStream instream = httpentity.getContent();
			result = convertStreamToString(instream);
			instream.close();
		}
		return result;
	}

	///Agrega los parametros a la url que recibe y los encodea con UTF8
	private String addParamsToURL(String url, HashMap<String, Object> params) throws UnsupportedEncodingException{
		if(params == null)
			return url;
		
		String urlConParametros;
		if(url.endsWith("?")){
			urlConParametros = url;
		} else{
			urlConParametros = url + "?";
		}
		int i = 0;
		for (String key : params.keySet()) {
			String value = "";
			Object rawValue = params.get(key);
			value += (String) key + "=" + URLEncoder.encode(String.valueOf(rawValue),"UTF-8");
			if (i > 0)
				urlConParametros += "&";
			urlConParametros += value;
			i++;
		}
		return urlConParametros;
	}
	
	///parsea la respuesta del request
	private String convertStreamToString(InputStream is) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(is), 8 * 1024);
		StringBuilder sb = new StringBuilder();
		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}
	
	private void Loggear(String url, HashMap<String, Object> params, String resultado){
		Log.d("HTTPUtils", "url: "+url);
		Log.d("HTTPUtils", "parametros: "+ (params!=null ? params.toString() : "null"));
		Log.d("HTTPUtils", "resultado: "+resultado);
	}
	
	private void postExecute(Respuesta rta, OnHTTPutilsResultListener listener){
		if(rta.getStatus()){
			listener.OnResult(rta.getResultado());
		}else{
			listener.OnFailed(rta.getResultado());
		}
	}
	
	private class URLParamsYListener{
		private String url;
		private HashMap<String, Object> params;
		private OnHTTPutilsResultListener listener;
		
		public URLParamsYListener(String urlrequest, HashMap<String, Object> parametros, OnHTTPutilsResultListener handler){
			url = urlrequest;
			params = parametros;
			listener = handler;
		}
		
		public String getUrl(){
			return url;
		}
		
		public HashMap<String, Object> getParams(){
			return params;
		}
		
		public OnHTTPutilsResultListener getListener(){
			return listener;
		}
	}
	
	private class URLParamsMultipartYlistener extends URLParamsYListener{
		private MultipartEntity multipartParams;
		
		public URLParamsMultipartYlistener(String urlrequest,
				HashMap<String, Object> urlParametros,
				MultipartEntity multipartParametros,
				OnHTTPutilsResultListener handler) {
			super(urlrequest, urlParametros, handler);
			multipartParams = multipartParametros;
		}
		
		public MultipartEntity getMultipartParams(){
			return multipartParams;
		}
	}
	
	
	private class Respuesta{
		private boolean status;
		private String resultado;
		
		public Respuesta(boolean pStatus, String pResultado){
			status = pStatus;
			resultado = pResultado;
		}
		
		public boolean getStatus(){
			return status;
		}
		public String getResultado(){
			return resultado;
		}
	}
	
}





